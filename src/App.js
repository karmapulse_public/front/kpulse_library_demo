import React from 'react';
import Board from 'kpulse_insights_lib';
import './App.css';

function App() {
  return (
    <Board karmaBoard="demo_seguridad_dev" env="develop" />
  );
}

export default App;
